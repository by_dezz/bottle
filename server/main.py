from web_core.app import App

import settings
from bottle.rotes import routes
from bottle import params_patterns


if __name__ == '__main__':
    app = App(settings, params_patterns, routes)
    app.run()
