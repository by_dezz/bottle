import json


with open('keys.json', 'r', encoding='UTF-8') as file:
    keys = json.load(file)


TORTOISE_ORM = {
    "connections": {
        "default": f'postgres://{keys["DB_LOGIN"]}:{keys["DB_PASSWORD"]}@localhost:5432/{keys["DB_NAME"]}'
        },

    "apps": {
        "bottle": {
            "models": ["bottle.database", "aerich.models"],
            "default_connection": "default",
        },
    },

    'use_tz': False,
    'timezone': 'Asia/Yekaterinburg'
}
