from aiohttp import web
import inspect

from aiohttp.web_response import json_response


class Pattern:
    result = None
    required = None

    @classmethod
    def can_load(cls, params):
        if not cls.required:
            return True

        for param in cls.required:
            if not params.get(param):
                return False

        return True

    @staticmethod
    async def load(params):
        return {}


class ParamsLoader:
    def __init__(self, raw_patterns):
        self.patterns = {}

        for name, obj in inspect.getmembers(raw_patterns):
            if inspect.isclass(obj) and issubclass(obj, Pattern) and name != 'Pattern':
                self.__load_pattern(obj)

    def __load_pattern(self, pattern):
        for param in pattern.result:
            self.patterns[param] = pattern

    def create_loaders_queue(self, signature_set):
        loaders_queue = set()
        intersection = signature_set.intersection(self.patterns.keys())
        while intersection:
            for param in intersection:
                signature_set.remove(param)
                signature_set.update(self.patterns[param].required)
                loaders_queue.add(param)

            intersection = signature_set.intersection(self.patterns.keys())

        return loaders_queue

    @staticmethod
    def load_defaults(all_params, signature_set, signature):
        for param in signature.parameters.values():
            if param.default != inspect.Parameter.empty:
                signature_set.remove(param.name)
                if not all_params.get(param.name):
                    all_params[param.name] = param.default

    async def load_from_queue(self, params_queue, all_params):
        index = 0
        params_queue = list(params_queue)
        while params_queue:
            loader = self.patterns[params_queue[index]]

            if loader.can_load(all_params):
                all_params.update(await loader.load(all_params))
                params_queue.remove(params_queue[index])
                if index == len(params_queue):
                    index = 0

            else:
                if index + 1 == len(params_queue):
                    index = 0
                else:
                    index += 1

    @staticmethod
    def insert_params(params, signature, all_params):
        for param in signature.parameters:
            value = all_params[param]
            if type(value) is str:
                if value.lower() == 'false':
                    value = False
                elif value.lower() == 'true':
                    value = True

            params[param] = value

    @staticmethod
    def get_all_params(request):
        all_params = dict(request.query)

        if getattr(request, 'user_data', None):
            all_params['user_data'] = request.user_data

        all_params['request'] = request

        return all_params

    @web.middleware
    async def middleware(self, request, handler):
        signature = inspect.signature(handler)
        all_params = self.get_all_params(request)

        params = {}
        signature_set = set(signature.parameters)
        loaders_queue = self.create_loaders_queue(signature_set)
        self.load_defaults(all_params, signature_set, signature)

        difference = signature_set.difference(all_params.keys())
        if difference:
            print(difference)
            return json_response({'error': 'Invalid params'}, status=400)

        await self.load_from_queue(loaders_queue, all_params)
        self.insert_params(params, signature, all_params)

        return await handler(**params)
