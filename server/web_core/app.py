import aerich
from aiohttp import web, web_runner
from tortoise import Tortoise

from . import middlewares
from .params_loader import ParamsLoader


class App:
    def __init__(self, settings, params_patterns, routes):
        params_loader = ParamsLoader(params_patterns)

        self.settings = settings
        self.app = web.Application()
        self.app.add_routes(routes)

        self.app.on_startup.extend([
            self.init_db,
        ])

        self.app.middlewares.extend([
            middlewares.slash_redirect,
            middlewares.print_request,
            params_loader.middleware,
        ])

        self.app.on_shutdown.append(self.on_shutdown)

    async def init_db(self, app):
        command = aerich.Command(self.settings.TORTOISE_ORM, app='bottle')
        await command.init()
        await command.migrate()

        await Tortoise.init(config=self.settings.TORTOISE_ORM)

    @staticmethod
    async def on_shutdown(app):
        raise web_runner.GracefulExit()

    def run(self):
        web.run_app(self.app)
