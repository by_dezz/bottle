from aiohttp import web

import inspect
import datetime


@web.middleware
async def slash_redirect(request, handler):
    if request.path[-1] != '/':
        raise web.HTTPFound(request.path + '/')

    response = await handler(request)
    return response


@web.middleware
async def print_request(request, handler):
    response = await handler(request)
    time_label = f'{datetime.datetime.today().replace(microsecond=0)}'
    print(f'[{time_label}] [{response.status}] {request.url}')
    return response


@web.middleware
async def args_loader(request, handler):
    params = dict(request.query)
    signature = inspect.signature(handler)

    kwargs = {}
    for param in signature.parameters.values():
        if param.name != 'request':
            if param.default is inspect.Parameter.empty and not params.get(param.name):
                return web.json_response({'error': 'Invalid params'}, status=400)

            elif params.get(param.name):
                if params[param.name] == 'True':
                    kwargs[param.name] = True

                elif params[param.name] == 'False':
                    kwargs[param.name] = False

                else:
                    kwargs[param.name] = params[param.name]

        else:
            kwargs[param.name] = request

    response = await handler(**kwargs)
    return response
